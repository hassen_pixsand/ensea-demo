import "./style.scss";

import { withFormik } from "formik";
import React, { useEffect, useState } from "react";
import { Navigate } from "react-router-dom";
import _ from "underscore";

import Button from "../../components/Button";
import { EntryPassword, EntryText } from "../../components/Entry";
import AuthLayout from "../../layout/AuthLayout";
// ...
import { validationSchema } from "./data";
const iniValues = validationSchema.cast();

import { sEntities, sSetAllItems } from "../../libs/storage";
import { USER_AUTH } from "../../services";

const SignIn = ({ user, ...props }) => {
  const [isLoading, setIsLoading] = useState(false);
  const [loginError, setLoginError] = useState("");
  const [errors, setErrors] = useState({});

  // ...
  const onValidate = async () => {
    return props.validateForm(props.values);
  };

  // ...
  const onTryLogin = async () => {
    const validate = await onValidate();
    if (!_.isEmpty(validate)) return setErrors({ ...validate });
    setIsLoading(true);
    // ...
    const { error, reason, data } = await USER_AUTH(props.values);
    setIsLoading(false);
    // ...
    if (error) {
      // TODO: display error
      console.log("#AUTH error:: ", reason);
    } else {
      // console.log("#AUTH data:: ", data);
      // ...
      sSetAllItems({
        [sEntities.JWT]: data?.jwt || "",
        [sEntities.USER]: JSON.stringify(data?.user || {}),
        [sEntities.PROG]: JSON.stringify(data?.programme || {}),
        [sEntities.CURSUS]: JSON.stringify(data?.cursus || {}),
      });
    }
  };

  // ...
  useEffect(() => {
    setLoginError("");
    setErrors({});
  }, [props.values]);

  // ...
  return (
    <>
      {user && <Navigate to="/dashboard/enseignements" replace={true} />}

      <AuthLayout width="450px">
        <div className="AuthSignIn">
          <div className="AuthLayout__content__header">
            <p className="AuthLayout__content__title">
              Log in for professionals
            </p>
            <p className="AuthLayout__content__desc">
              Access your partner account to manage your business
            </p>
          </div>

          <div className="AuthLayout__content__form AuthSignIn__form">
            <EntryText
              name="identifier"
              label="Email"
              value={props.values.identifier}
              onChange={props.handleChange}
              error={errors.identifier}
            />

            <EntryPassword
              name="password"
              label="Password"
              value={props.values.password}
              onChange={props.handleChange}
              error={errors.password}
            />

            {!!loginError && <p className="__error">{loginError}</p>}

            <Button fontSize="16px" isLoading={isLoading} onClick={onTryLogin}>
              Sign in
            </Button>
          </div>
        </div>
      </AuthLayout>
    </>
  );
};

export default withFormik({
  mapPropsToValues: () => ({ ...iniValues }),
  validationSchema,
})(SignIn);
