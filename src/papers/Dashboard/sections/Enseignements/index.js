import "./style.scss";

import React, { useState } from "react";

// ...
import Button from "../../../../components/Button";
import Container from "../../../../components/Container";
import EnsItem from "../../../../components/EnsItem";

const Enseignements = ({ user, data, isDataLoading }) => {
  const [isLoading, setIsLoading] = useState(false);
  // ...
  const CURSUS = data?.CURSUS || {};
  const PROG = data?.PROG;
  // ...
  const cLabel = CURSUS?.Libelle || "";
  const pLabel = PROG?.Libelle || "";
  const ensList = PROG?.ENS_ET_MOD_INS || [];
  // ...
  // console.log("#BASE cursus :: ", CURSUS);
  // console.log("#BASE prog :: ", ensList);
  // ...
  return (
    <Container variant="v976">
      <div className="DashEnseignements">
        <div className="DashEnseignements__header">
          <div>
            <h2>CURSUS: {cLabel}</h2>
            <p>{pLabel}</p>
          </div>
          <div className="DashEnseignements__header__action">
            <Button width="150px" isLoading={isLoading}>
              Save
            </Button>
          </div>
        </div>

        <div className="DashEnseignements__content">
          {ensList.map((data, i) => (
            <EnsItem key={i} data={data} />
          ))}
        </div>
      </div>
    </Container>
  );
};

export default Enseignements;
