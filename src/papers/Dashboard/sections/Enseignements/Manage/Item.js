import Checkbox from "@mui/material/Checkbox";
import React from "react";

const Item = ({ data, checked, onCheckHandler }) => {
  console.log("#ENS item :: ", data?.option);
  const opt = data?.option || {};
  const label = opt?.Libelle || "";
  const ph = opt?.Poid_horaire || "";
  const tm = opt?.Thematique || "";
  // ...
  return (
    <div className="OptionItem">
      <div className="__inner" onClick={() => onCheckHandler(data)}>
        <div className="__checkbox">
          <Checkbox
            checked={checked}
            onChange={() => onCheckHandler(data)}
            inputProps={{ "aria-label": "controlled" }}
          />
        </div>
        <div className="__content">
          <div className="__top">
            <span className="__label">{label}</span>
            <span className="__ph">{ph} hours</span>
          </div>
          <div className="__bottom">{tm}</div>
        </div>
      </div>
    </div>
  );
};

export default Item;
