import ListAltIcon from "@mui/icons-material/ListAlt";
import update from "immutability-helper";
import React, { useCallback } from "react";
import { useDrop } from "react-dnd";

import Card from "./Card.js";
import { ItemTypes } from "./ItemTypes.js";

const DragList = ({ cards, setCards }) => {
  const [xxx, drop] = useDrop(() => ({
    accept: ItemTypes.CARD,
  }));
  console.log("#DRAG :: ", xxx);
  // ...
  const findCardHandler = useCallback(
    (id) => {
      const card = cards.filter((c) => `${c.id}` === id)[0];
      return {
        card,
        index: cards.indexOf(card),
      };
    },
    [cards]
  );
  // ...
  const moveCardHandler = useCallback(
    (id, atIndex) => {
      const { card, index } = findCardHandler(id);
      setCards(
        update(cards, {
          $splice: [
            [index, 1],
            [atIndex, 0, card],
          ],
        })
      );
    },
    [findCardHandler, cards, setCards]
  );
  // ...
  return (
    <div ref={drop} className="DragList">
      {cards.map((card) => (
        <Card
          key={card.id}
          id={`${card.id}`}
          data={card}
          moveCardHandler={moveCardHandler}
          findCardHandler={findCardHandler}
        />
      ))}

      {cards?.length === 0 && (
        <div className="DragListEmpty">
          <ListAltIcon /> <span>Select your options</span>
        </div>
      )}
    </div>
  );
};

export default DragList;
