import "./style.scss";

import InfoIcon from "@mui/icons-material/Info";
import React, { useEffect, useState } from "react";
import { DndProvider } from "react-dnd";
import { HTML5Backend } from "react-dnd-html5-backend";
import { useNavigate, useParams } from "react-router-dom";

import ManageBox from "../../../../../components/ManageBox";
import { sGetAllItems } from "../../../../../libs/storage";
import DragList from "./DragList";
import Item from "./Item";

const DashEnseignementsManage = () => {
  const [BASE_ENS, SET_BASE_ENS] = useState({});
  // ...
  const navigate = useNavigate();
  const params = useParams();
  const eId = params?.id;
  // ...
  const [done, setDone] = useState("");
  const [cards, setCards] = useState([]);
  const [options, setOptions] = useState([]);
  const [rulesOpts, setRulesOpts] = useState({ phs: 0 });

  // ...
  const title = BASE_ENS.Libelle || "";
  const canNext = true;
  const isLoading = false;
  const fallbackRoute = "/dashboard/enseignements";
  // ...
  const onCheckHandler = (o) => {
    const isIn = options.indexOf(o.id) > -1;
    if (!isIn) setOptions((s) => [...s, o.id]);
    else setOptions((s) => s.filter((oId) => oId !== o.id));
  };
  // ...
  const onSave = () => {
    if (done) return;
    setDone("save");
    // ...
    navigate(fallbackRoute, { replace: true });
  };
  // ...
  useEffect(() => {
    let sel = [];

    //# Get Selected Options
    options.forEach((oId) => {
      _BASE.forEach((opt) => {
        if (oId === opt.id) sel.push(opt);
      });
    });

    //# Get Rules
    const phs =
      sel.map((o) => o.option.Poid_horaire).reduce((x, y) => x + y, 0) || 0;

    // ...
    setCards(sel);
    setRulesOpts((s) => ({ ...s, phs }));
  }, [options]);
  // ...
  useEffect(() => {
    if (!eId) return;
    // ...
    const raw = sGetAllItems();
    let ensTarget = {};
    // ...
    raw?.PROG?.ENS_ET_MOD_INS?.map((ens) => {
      if (eId === ens.id.toString()) ensTarget = ens;
    });
    // ...
    SET_BASE_ENS(ensTarget);
  }, [eId]);
  // ...
  const _BASE = BASE_ENS?.ENS?.Options || [];
  // ...
  console.log("#Rules :: ", rulesOpts);
  // ...
  return (
    <ManageBox
      title={title}
      canNext={canNext}
      isLoading={isLoading}
      actions={{ onSave, close: fallbackRoute }}
    >
      <h2>{title}</h2>

      <div className="DashEnseignementsManage">
        <div className="DashEnseignementsManage__item DashEnseignementsManage__base">
          <div className="__header">
            <InfoIcon />
            <span>Available options</span>
          </div>
          <div className="__body">
            {_BASE.map((card, index) => (
              <Item
                key={index}
                data={card}
                checked={options.indexOf(card.id) > -1}
                onCheckHandler={onCheckHandler}
              />
            ))}
          </div>
        </div>

        <div className="DashEnseignementsManage__sep" />

        <div className="DashEnseignementsManage__item DashEnseignementsManage__selected">
          <div className="__header">
            <InfoIcon />
            <span>Selected options</span>
          </div>
          <div className="__body">
            <DndProvider backend={HTML5Backend}>
              <DragList cards={cards} setCards={setCards} />
            </DndProvider>
          </div>
        </div>
      </div>
    </ManageBox>
  );
};

export default DashEnseignementsManage;
