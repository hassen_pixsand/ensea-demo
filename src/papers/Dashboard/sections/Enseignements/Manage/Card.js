import React from "react";
import { useDrag, useDrop } from "react-dnd";

const ItemTypes = { CARD: "card" };

const Card = ({ id, data, moveCardHandler, findCardHandler }) => {
  const originalIndex = findCardHandler(id).index;
  // ...
  const [{ isDragging }, drag] = useDrag(
    () => ({
      type: ItemTypes.CARD,
      item: { id, originalIndex },
      collect: (monitor) => ({
        isDragging: monitor.isDragging(),
      }),
      end: (item, monitor) => {
        const { id: droppedId, originalIndex } = item;
        const didDrop = monitor.didDrop();
        if (!didDrop) moveCardHandler(droppedId, originalIndex);
      },
    }),
    [id, originalIndex, moveCardHandler]
  );
  // ...
  const [, drop] = useDrop(
    () => ({
      accept: ItemTypes.CARD,
      hover({ id: draggedId }) {
        if (draggedId !== id) {
          const { index: overIndex } = findCardHandler(id);
          moveCardHandler(draggedId, overIndex);
        }
      },
    }),
    [findCardHandler, moveCardHandler]
  );
  // ...
  const opt = data?.option || {};
  const label = opt?.Libelle || "";
  const ph = opt?.Poid_horaire || "";
  const tm = opt?.Thematique || "";
  // ...
  return (
    <div
      ref={(node) => drag(drop(node))}
      className="OptionItem DragItem"
      style={{ opacity: isDragging ? 0 : 1 }}
    >
      <div className="__inner">
        <div className="__content">
          <div className="__top">
            <span className="__label">{label}</span>
            <span className="__ph">{ph} hours</span>
          </div>
          <div className="__bottom">{tm}</div>
        </div>
      </div>
    </div>
  );
};

export default Card;
