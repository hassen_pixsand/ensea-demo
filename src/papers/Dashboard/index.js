import React, { useEffect, useState } from "react";
import { Navigate, Route, Routes } from "react-router-dom";
import _ from "underscore";

import { sGetAllItems } from "../../libs/storage";
import None from "../None";
import routes from "./routes";

const Dashboard = ({ user }) => {
  const [data, setData] = useState({});
  const [isLoading, setIsLoading] = useState(true);
  const isEmptyData = _.isEmpty(data);

  // ...
  useEffect(() => {
    if (!isEmptyData) return setIsLoading(false);
    setIsLoading(true);
    // ...
    setData(sGetAllItems());
    setIsLoading(false);
  }, [user, data]);
  // ...
  return (
    <>
      {!user && <Navigate to="/authentication" replace={true} />}

      <Routes>
        {routes().map((route) => (
          <Route
            key={route.id}
            path={route.path}
            element={
              <route.Component
                {...route.props}
                // ...
                user={user}
                data={data}
                isDataLoading={isLoading}
              />
            }
          />
        ))}

        <Route path="*" element={<None user={user} />} />
      </Routes>
    </>
  );
};

export default Dashboard;
