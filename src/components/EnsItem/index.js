import "./style.scss";

import React from "react";
import Moment from "react-moment";

import NavLink from "../NavLink";

const EnsItem = ({ data }) => {
  // console.log("#BASE item :: ", data);
  // ...
  const iId = data?.id;
  const iLabel = data?.Libelle;
  // ...
  const iENS = data?.ENS || {};
  const optsL = iENS?.Options?.length || 0;
  let optsLabel = "No options";
  if (optsL === 1) optsLabel = "1 option";
  if (optsL > 1) optsLabel = `${optsL} options`;
  // ...
  const canOnline = !!data?.Inscription_en_ligne;
  // ...
  const startD = data?.Date_ouverture_inscritption;
  const endD = data?.Date_fermeture_inscritption;
  const hasDateRange = startD && endD;
  // ...
  return (
    <div className="EnsItem">
      <NavLink to={`/dashboard/enseignements/${iId}`}>
        <div className="EnsItem__inner">
          <div className="__col-1">
            <div className="__name">{iLabel}</div>
            <div className="__sub">{optsLabel}</div>
          </div>
          <div className="__col-2">
            {canOnline ? "Online registration" : ""}
          </div>
          {hasDateRange ? (
            <div className="__col-3">
              <span>
                <Moment format="MMM D YYYY">{startD}</Moment>
              </span>
              <span> - </span>
              <span>
                <Moment format="MMM D YYYY">{endD}</Moment>
              </span>
            </div>
          ) : (
            <div className="__col-3" />
          )}
        </div>
      </NavLink>
    </div>
  );
};

export default EnsItem;
