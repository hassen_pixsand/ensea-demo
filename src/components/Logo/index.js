import "./style.scss";

import React from "react";

// import Svg from "../Svg";

const Logo = ({ style }) => (
  <div className="Logo" style={{ ...style }}>
    {/* <Svg src="/logo.svg" /> */}
    <span className="__tmp">LOGO</span>
  </div>
);

export default Logo;
