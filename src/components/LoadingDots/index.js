import "./style.scss";

import cx from "classnames";
import React from "react";

const index = ({ type }) => {
  return (
    <div className="ball-loader">
      <div className={cx("ball-loader-ball ball1", { [type]: !!type })} />
      <div className={cx("ball-loader-ball ball2", { [type]: !!type })} />
      <div className={cx("ball-loader-ball ball3", { [type]: !!type })} />
    </div>
  );
};

export default index;
