import Box from "@material-ui/core/Box";
import cx from "classnames";
import MuiPhoneNumber from "material-ui-phone-number";
import { useTracker } from "meteor/react-meteor-data";
import React from "react";

import SaasCollection from "/imports/api/Saas";

const EntryPhone = ({
  className,
  // ...
  name,
  label,
  value,
  onChange,
  onBlur,
  readOnly,
  // ...
  error,
  customError,
}) => {
  let helperText = error ? `${label} is required` : "";
  if (error && customError) helperText = error;
  // ...
  const onChangeHandler = (val) => {
    const value = (val || "").replace(/\s/g, "");
    onChange(name, value);
  };
  // ...
  const { country } = useTracker(() => {
    const saas = SaasCollection.findOne();
    return { country: (saas?.country || "Asia/Dubai").toLowerCase() };
  });
  // ...
  return (
    <Box
      className={cx("Entry EntryPhone", {
        [className]: !!className,
      })}
    >
      <MuiPhoneNumber
        variant="outlined"
        fullWidth
        defaultCountry={country}
        autoFormat={true}
        dropdownClass="EntryPhoneList"
        // ...
        name={name}
        label={label}
        value={value}
        error={!!error}
        helperText={helperText}
        onChange={onChangeHandler}
        onBlur={onBlur}
        // ...
        disabled={!!readOnly}
      />
    </Box>
  );
};

export default EntryPhone;
