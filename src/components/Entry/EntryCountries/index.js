import Box from "@material-ui/core/Box";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@mui/material/InputLabel";
import cx from "classnames";
import React from "react";
import ReactFlagsSelect from "react-flags-select";

import { supportedCountries } from "/imports/libs/countries";

const EntryCountries = ({
  className,
  name,
  label,
  value,
  onChange,
  disabled,
}) => {
  // ...
  return (
    <Box className={cx("Entry EntryCountries", { [className]: className })}>
      <FormControl fullWidth>
        <InputLabel className="__label">{label}</InputLabel>
        <ReactFlagsSelect
          className="__entry"
          name={name}
          label={label}
          // ...
          selected={value}
          onSelect={(code) => {
            onChange(name, code);
          }}
          // ...
          // searchable
          // searchPlaceholder="Search countries"
          // ...
          countries={supportedCountries}
          showSelectedLabel={true}
          // ...
          disabled={disabled}
        />
      </FormControl>
    </Box>
  );
};

export default EntryCountries;
