import Box from "@material-ui/core/Box";
import FormControl from "@material-ui/core/FormControl";
import Slider from "@material-ui/core/Slider";
import React from "react";

const marks = [
  {
    label: "Low",
    value: 0,
  },
  {
    label: "Medium",
    value: 1,
  },
  {
    label: "High",
    value: 2,
  },
];

const EntrySlider = ({ value = 0, onInputChange, readOnly }) => {
  const onChange = (e, value) => {
    if (readOnly) return;
    onInputChange(value);
  };
  // ...
  return (
    <Box className="EntrySlider">
      <FormControl className="EntrySlider">
        <Slider
          component="div"
          aria-labelledby="discrete-slider"
          // value={value}
          getAriaValueText={(value) => value}
          // valueLabelDisplay="on"
          marks={marks}
          // onChange={onChange}
          min={0}
          max={2}
        />
      </FormControl>
    </Box>
  );
};

export default EntrySlider;
