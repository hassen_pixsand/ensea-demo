import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Fade from "@material-ui/core/Fade";
import InputAdornment from "@material-ui/core/InputAdornment";
import Paper from "@material-ui/core/Paper";
import TextField from "@material-ui/core/TextField";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import cx from "classnames";
import React, { useState } from "react";

import { colors } from "/imports/libs/entries";

const EntryColor = ({
  className = "",
  name,
  label,
  value,
  onChange,
  onBlur,
  error,
}) => {
  const [open, setOpen] = useState(false);
  // ...
  const onColorChange = (color) => {
    onChange(name, color);
    onBlur(name, true);
    // ...
    setOpen(false);
  };
  // ...
  return (
    <ClickAwayListener onClickAway={() => setOpen(false)}>
      <div
        className={cx("Entry EntryText EntryColor", {
          [className]: className,
        })}
      >
        <TextField
          variant="outlined"
          // ...
          name={name}
          label={label}
          // ...
          onClick={() => setOpen((state) => !state)}
          // ...
          fullWidth
          error={!!error}
          helperText={error ? `${label} is required` : ""}
          InputLabelProps={{ shrink: true }}
          InputProps={{
            readOnly: true,
            startAdornment: (
              <InputAdornment position="start">
                <div style={{ backgroundColor: value }} />
              </InputAdornment>
            ),
            endAdornment: <ArrowDropDownIcon position="start" />,
          }}
        />

        {open && (
          <Fade in={open}>
            <Paper className="EntryColor__dropdown" elevation={0}>
              {colors.map((color, idx) => (
                <div
                  key={idx}
                  className="EntryColor__item"
                  style={{ backgroundColor: color }}
                  onClick={() => onColorChange(color)}
                />
              ))}
            </Paper>
          </Fade>
        )}
      </div>
    </ClickAwayListener>
  );
};

export default EntryColor;
