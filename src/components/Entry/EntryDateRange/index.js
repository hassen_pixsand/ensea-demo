import "react-date-range/dist/styles.css";
import "react-date-range/dist/theme/default.css";

import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Fade from "@material-ui/core/Fade";
import Paper from "@material-ui/core/Paper";
import { addDays } from "date-fns";
import React from "react";
import { DateRangePicker } from "react-date-range";

const EntryDateRange = ({
  openPicker,
  setOpenPicker,
  // ...
  selection,
  handler,
  // ...
  style = {},
  children,
}) => {
  // ...
  return (
    <ClickAwayListener onClickAway={() => setOpenPicker(false)}>
      <div className="EntryDateRange">
        <div
          className="EntryDateRange__trigger"
          onClick={() => setOpenPicker(!openPicker)}
        >
          {children}
        </div>

        {openPicker && (
          <Fade in={openPicker}>
            <Paper
              className="EntryDateRange__dropdown"
              elevation={0}
              style={{ ...style }}
            >
              <div className="EntryDateRange__picker">
                <DateRangePicker
                  direction="vertical"
                  onChange={handler}
                  ranges={[selection]}
                  // ...
                  months={1}
                  minDate={addDays(new Date(), -300)}
                  maxDate={addDays(new Date(), 900)}
                  // ...
                  scroll={{ enabled: true }}
                  rangeColors={["#fab207"]}
                  // ...
                  // disabled={true}
                />
              </div>
            </Paper>
          </Fade>
        )}
      </div>
    </ClickAwayListener>
  );
};

export default EntryDateRange;
