import InputAdornment from "@material-ui/core/InputAdornment";
import TextField from "@material-ui/core/TextField";
import cx from "classnames";
import React from "react";

const EntryText = ({
  className = "",
  type = "text",
  name,
  label,
  placeholder = "",
  value,
  onChange,
  onBlur,
  error,
  // ...
  readOnly,
  multiline,
  rows,
  // ...
  unit,
}) => {
  return (
    <div
      className={cx("Entry EntryText EntryTextUnit", {
        [className]: className,
      })}
    >
      <TextField
        variant="outlined"
        // ...
        type={type}
        name={name}
        label={label}
        placeholder={placeholder}
        value={value}
        onChange={onChange}
        onBlur={onBlur}
        multiline={multiline}
        rows={rows}
        fullWidth
        error={!!error}
        helperText={error ? `${label} is required` : ""}
        InputLabelProps={{ shrink: true }}
        InputProps={{
          readOnly,
          startAdornment: (
            <InputAdornment position="start">{unit}</InputAdornment>
          ),
        }}
      />
    </div>
  );
};

export default EntryText;
