import cx from "classnames";
import ColorPicker from "material-ui-color-picker";
import React from "react";

const EntryColorNew = ({ className = "", name, label, value, onChange }) => {
  const onColorChange = (color) => {
    onChange(name, color);
  };
  // ...
  return (
    <div
      className={cx("Entry EntryColorNew", {
        [className]: className,
      })}
    >
      <div className="EntryColorNew__preview">
        <div
          className="EntryColorNew__preview__inner"
          style={{ backgroundColor: value }}
        />
      </div>
      <div className="EntryColorNew__picker">
        <ColorPicker
          name={name}
          label={label}
          value={value}
          // defaultValue={value}
          onChange={onColorChange}
          // ...
          fullWidth
          error={true}
          helperText="hhhh"
        />
      </div>
    </div>
  );
};

export default EntryColorNew;
