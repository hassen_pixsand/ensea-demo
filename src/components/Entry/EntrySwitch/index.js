import FormControlLabel from "@material-ui/core/FormControlLabel";
import Switch from "@material-ui/core/Switch";
import React from "react";

const EntrySwitch = ({
  name,
  label,
  value,
  onChange,
  onBlur,
  labelPlacement = "end",
  disabled,
}) => {
  return (
    <div className="Entry EntrySwitch">
      <FormControlLabel
        label={label}
        labelPlacement={labelPlacement}
        disableRipple
        control={
          <Switch
            name={name}
            checked={value}
            onChange={onChange}
            onBlur={onBlur}
            // ...
            disabled={disabled}
          />
        }
      />
    </div>
  );
};

export default EntrySwitch;
