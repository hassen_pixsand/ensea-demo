import FormControl from "@material-ui/core/FormControl";
import FormHelperText from "@material-ui/core/FormHelperText";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import cx from "classnames";
import React from "react";

const EntrySelect = ({
  className = "",
  name,
  label,
  placeholder = "",
  value,
  onChange,
  onBlur,
  // ...
  isRessources,
  error,
  // ...
  disabled,
  style = {},
  // ...
  options = [],
}) => {
  return (
    <div
      className={cx(
        "Entry",
        "EntrySelect",
        { [className]: !!className },
        { __placeholder: !value }
      )}
      style={{ ...style }}
    >
      <FormControl variant="outlined" disabled={disabled} error={!!error}>
        <InputLabel shrink={true}>{label}</InputLabel>
        <Select
          native
          value={value}
          onChange={onChange}
          onBlur={onBlur}
          // InputLabelProps={{ shrink: true }}
          inputProps={{ name }}
        >
          {!!placeholder && (
            <option aria-label="None" value="" style={{ color: "#b5b5b5" }}>
              {placeholder}
            </option>
          )}

          {options.map((option) => (
            <option key={option.value} value={option.value}>
              {option.label}
            </option>
          ))}
        </Select>
        {error && (
          <FormHelperText style={{ color: "#f44336" }}>
            {isRessources ? error : `${label} is required`}
          </FormHelperText>
        )}
      </FormControl>
    </div>
  );
};

export default EntrySelect;
