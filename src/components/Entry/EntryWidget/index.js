import InputAdornment from "@material-ui/core/InputAdornment";
import { withStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Tooltip from "@material-ui/core/Tooltip";
import FileCopyIcon from "@material-ui/icons/FileCopy";
import cx from "classnames";
import React, { useEffect, useState } from "react";
import { CopyToClipboard } from "react-copy-to-clipboard";

const CopyTooltip = withStyles(() => ({
  popper: {
    margin: "27px 10px !important",
  },
}))(Tooltip);

const EntryText = ({
  className = "",
  name,
  label,
  placeholder = "",
  value,
  onChange,
  onBlur,
  error,
  // ...
  readOnly,
  multiline,
  rows,
}) => {
  const [isCopied, setIsCopied] = useState(false);
  // ...
  useEffect(() => {
    if (!isCopied) return;
    // ...
    setTimeout(() => {
      setIsCopied(false);
    }, 500);
  }, [isCopied]);
  // ...
  return (
    <div
      className={cx("Entry EntryText EntryTextUnit EntryTextWidget", {
        [className]: className,
      })}
    >
      <TextField
        variant="outlined"
        // ...
        type="text"
        name={name}
        label={label}
        placeholder={placeholder}
        value={value}
        onChange={onChange}
        onBlur={onBlur}
        multiline={multiline}
        rows={rows}
        fullWidth
        error={!!error}
        helperText={error ? `${error}` : ""}
        InputLabelProps={{ shrink: true }}
        InputProps={{
          readOnly,
          startAdornment: (
            <InputAdornment position="start">book.sporta.co</InputAdornment>
          ),
          endAdornment: (
            <InputAdornment position="end">
              <CopyTooltip
                open={isCopied}
                arrow
                placement="bottom"
                title="Copied!"
              >
                <CopyToClipboard
                  text={`https://book.sporta.co/${value}`}
                  onCopy={() => setIsCopied(true)}
                >
                  <div>
                    <FileCopyIcon />
                  </div>
                </CopyToClipboard>
              </CopyTooltip>
            </InputAdornment>
          ),
        }}
      />
    </div>
  );
};

export default EntryText;
