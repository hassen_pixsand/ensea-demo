import "date-fns";

import DateFnsUtils from "@date-io/date-fns";
import Box from "@material-ui/core/Box";
// import FormHelperText from "@material-ui/core/FormHelperText";
import {
  KeyboardDatePicker,
  MuiPickersUtilsProvider,
} from "@material-ui/pickers";
import cx from "classnames";
import React from "react";

const EntryDate = ({
  className,
  variant = "modal",
  // ...
  name,
  label,
  value,
  placeholder,
  // ...
  minDate,
  maxDate,
  readOnly,
  // ...
  onChange,
  onBlur,
  error,
}) => {
  // ...
  return (
    <Box className={cx("Entry EntryDate", { [className]: !!className })}>
      <MuiPickersUtilsProvider utils={DateFnsUtils}>
        <KeyboardDatePicker
          // autoOk
          variant={variant}
          DialogProps={{ class: "EntryDateContainer" }}
          // PopoverProps={{ class: "_________________b" }}
          inputVariant="outlined"
          InputLabelProps={{ shrink: true }}
          placeholder={placeholder}
          fullWidth
          // clearable
          okLabel="Save"
          margin="normal"
          views={["year", "month", "date"]}
          openTo="year"
          KeyboardButtonProps={{
            "aria-label": "change date",
          }}
          //...
          // disableToolbar
          minDate={minDate}
          maxDate={maxDate}
          disabled={readOnly}
          // ...
          label={label}
          format="d MMMM yyyy"
          name={name}
          value={value}
          onChange={onChange}
          onBlur={onBlur}
          // ...
          error={!!error}
          helperText={error ? `${label} is required` : ""}
        />
      </MuiPickersUtilsProvider>
    </Box>
  );
};

export default EntryDate;
