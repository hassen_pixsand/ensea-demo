import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import React from "react";

const EntryCheckbox = ({
  name,
  label,
  value,
  onChange,
  onBlur,
  labelPlacement = "end",
  indeterminate,
  disabled,
}) => {
  // ...
  return (
    <div className="Entry EntryCheckbox">
      <FormControlLabel
        label={label}
        labelPlacement={labelPlacement}
        disabled={disabled}
        control={
          <Checkbox
            name={name}
            checked={value}
            value={value}
            onChange={onChange}
            onBlur={onBlur}
            indeterminate={indeterminate}
            disabled={disabled}
            disableRipple
          />
        }
      />
    </div>
  );
};

export default EntryCheckbox;
