import CurrencyTextField from "@unicef/material-ui-currency-textfield";
import cx from "classnames";
import { useTracker } from "meteor/react-meteor-data";
import React from "react";

import SaasCollection from "/imports/api/Saas";

const EntryCurrency = ({
  className = "",
  name,
  label,
  placeholder = "",
  value,
  onChange,
  onBlur,
  error,
  // ...
  minimumValue,
  maximumValue,
  // ...
  readOnly,
  // ...
  baseCurrency,
}) => {
  const { currency } = useTracker(() => {
    const saas = SaasCollection.findOne();
    return { currency: saas?.currency || "AED" };
  });
  // ...
  return (
    <div
      className={cx("Entry EntryText EntryCurrency", {
        [className]: className,
      })}
    >
      <CurrencyTextField
        variant="outlined"
        textAlign="left"
        // ...
        name={name}
        label={label}
        placeholder={placeholder}
        value={value}
        onChange={(e, v) => {
          onChange(name, v);
        }}
        onBlur={onBlur}
        error={!!error}
        helperText={error ? `${label} is required` : ""}
        InputLabelProps={{ shrink: true }}
        readOnly={!!readOnly}
        disabled={!!readOnly}
        // ...
        leadingZero="deny"
        currencySymbol={baseCurrency || currency}
        // ...
        minimumValue={minimumValue || "0"}
        maximumValue={maximumValue || "9999999999999"}
        outputFormat="string"
        decimalPlaces={2}
        decimalCharacter="."
        digitGroupSeparator=","
        decimalPlacesShownOnFocus={2}
        decimalPlacesShownOnBlur={2}
      />
    </div>
  );
};

export default EntryCurrency;
