import Box from "@material-ui/core/Box";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";
import cx from "classnames";
import React from "react";

const RenderInput = ({ params, label, placeholder }) => (
  <TextField
    {...params}
    variant="outlined"
    label={label}
    placeholder={placeholder}
    InputLabelProps={{ shrink: true }}
  />
);

const NoOptionsText = () => (
  <span className="MuiAutocomplete-NoOption">No option found</span>
);

const EntryAutocomplete = ({
  className,
  name,
  label,
  placeholder = "",
  value = [],
  options,
  // ...
  onChange,
  onBlur,
}) => {
  return (
    <Box
      className={cx("Entry", "EntryAutocomplete", { [className]: !!className })}
    >
      <Autocomplete
        multiple
        name={name}
        value={value}
        options={options}
        onChange={onChange}
        onBlur={onBlur}
        filterSelectedOptions
        // ...
        getOptionLabel={(opt) => {
          const idx = _.indexOf(_.pluck(options, "value"), opt?.value || opt);
          return options?.[idx]?.label || "Label not found";
        }}
        getOptionSelected={(option) => value.indexOf(option?.value) > -1}
        // ...
        renderInput={(params) => (
          <RenderInput
            params={params}
            label={label}
            placeholder={placeholder}
          />
        )}
        noOptionsText={<NoOptionsText />}
      />
    </Box>
  );
};

export default EntryAutocomplete;
