import "./style.scss";

import React, { useState } from "react";
import { useNavigate } from "react-router-dom";

import { sClearAllItems } from "../../libs/storage";
// import Avatar from "/imports/ui/common/Avatar";
// import MenuUser from "/imports/ui/common/MenuUser";

const NavbarUser = ({ user }) => {
  console.log("#USR :: ", user);
  const history = useNavigate();
  const [userMenu, setUserMenu] = useState(false);
  // ...
  const fn = user?.profile?.firstName || "C";
  const ln = user?.profile?.lastName || "";
  // ...
  const onNavigate = (link) => {
    history.push(link);
  };
  const onLogout = () => {
    sClearAllItems();
  };
  // ...
  return (
    <div className="NavbarUser">
      {/* <div
        className={cx("NavbarUser__notifications", { __new: hasNewNotif })}
        onClick={() => onOpenNotif()}
      >
        <SVG src="/dashboard/topbar/notifications.svg" />
      </div> */}

      <div className="NavbarUser__user" onClick={() => onLogout()}>
        <div className="__user__tmp">
          <span>S</span>
        </div>
        {/* <MenuUser
          user={user}
          selectOpen={userMenu}
          setSelectOpen={setUserMenu}
          style={{
            top: "70px",
            width: "250px",
            border: "1px solid rgba(0, 0, 0, 0.1)",
            borderRadius: "6px",
            boxShadow: "0px 12px 16px rgba(181, 181, 181, 0.15)",
          }}
          options={[
            {
              label: "Dashboard",
              handler: () => onNavigate("/dashboard"),
            },
            {
              label: "Logout",
              handler: onLogout,
            },
          ]}
        >
          <Avatar small label={`${fn} ${ln}`} fileId={user?.imgId} />
        </MenuUser> */}
      </div>
    </div>
  );
};

export default NavbarUser;
