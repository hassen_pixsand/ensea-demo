import axios from "axios";

import { sEntities, sGetItem } from "../libs/storage";

//# Inst
const HTTP = axios.create({
  baseURL: "http://backoffice.paris-lavillette.archi.fr/api",
  // timeout: 5000,
  // headers: { "X-Custom-Header": "foobar" },
});

// Inst. Requests
HTTP.interceptors.request.use(
  async (config) => {
    config.headers.Accept = "application/json";
    config.headers["Content-Type"] = "application/json";
    // ...
    const jwt = sGetItem(sEntities.JWT);
    if (jwt) config.headers.Authorization = `Bearer ${jwt}`;
    else config.headers.Authorization = null;
    // ...
    return config;
  },
  (error) => {
    Promise.reject(error);
  }
);

//# Auth APIs
export const USER_AUTH = async (req) => {
  try {
    const res = await HTTP.post("/auth/local", req);
    const data = res.data;
    // ...
    return { error: false, reason: "", data };
  } catch (err) {
    return {
      error: true,
      reason: err?.code || "ERROR",
      data: null,
    };
  }
};

//# Ens APIs
export const GET_ENS = async () => {
  try {
    const res = await HTTP.get("/ensssss");
    const data = res.data;
    // ...
    return { error: false, reason: "", data };
  } catch (err) {
    return {
      error: true,
      reason: err?.code || "ERROR",
      data: null,
    };
  }
};

//# Selections
export const POST_SEL = async ({ req }) => {
  try {
    const res = HTTP.post("/dsfdsfddfs", req);
    const data = res.data;
    // ...
    return { error: false, reason: "", data };
  } catch (err) {
    return {
      error: true,
      reason: err?.code || "ERROR",
      data: null,
    };
  }
};
