export const AUTH_DATA = { selector: "student@dev.dev", password: "dev123" };

export const ENS_DATA = [
  {
    title: "Enseignement 1",
    sub: "Only used for a Demo",
    extra: "Extra details",
    last: "Other details",
  },
  {
    title: "Enseignement 2",
    sub: "Only used for a Demo",
    extra: "Extra details",
    last: "Other details",
  },
  {
    title: "Enseignement 3",
    sub: "Only used for a Demo",
    extra: "Extra details",
    last: "Other details",
  },
  {
    title: "Enseignement 4",
    sub: "Only used for a Demo",
    extra: "Extra details",
    last: "Other details",
  },
];

export const OPTS_DATA = [];
